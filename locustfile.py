from locust import HttpUser, task, between

class NumericalIntegrationUser(HttpUser):
    wait_time = between(1, 3)
    #host = "http://localhost:5000" # part1
    #host="http://localhost:80"
    #host = "http://20.228.183.10" # part2
    host="https://webappimenchihaoui.azurewebsites.net" # part3
    #host="https://appfuncimench.azurewebsites.net/api" # part 4
    @task
    def numerical_integration(self):
        self.client.get("/numericalintegralservice/0/3.14159") # part 1 & 2 & 3
        #self.client.get("/numericalintegralservice?lower=0&upper=3.149") # part 4

