import os
import pandas as pd
import matplotlib.pyplot as plt

# Initialize lists to store data for each file
file_path = 'C:/Users/imen/Desktop/final/part3_stats_history.csv'

# Read the CSV file into a pandas DataFrame
df = pd.read_csv(file_path)


# Convert the timestamp column to datetime format

# Set the start point of the timestamp column to 0
df['Timestamp'] = df['Timestamp'] - df['Timestamp'][0]

# Select the data starting from 25 seconds before
start_time_offset = 0 #25
df_first_180s = df[df['Timestamp'] <= 180]

plt.plot(df_first_180s['Timestamp'], df_first_180s['Requests/s'], label=' Requests/s', linestyle='-', color='green')
plt.plot(df_first_180s['Timestamp'], df_first_180s['Failures/s'], label=' Failures/s', linestyle='--', color='orange')

# Add labels, title, legend, and grid
plt.xlabel('Time (seconds)')
plt.ylabel('Count')
plt.title('Requests and Failures per second')
plt.legend()
plt.grid(True)

# Set x-axis limits to start from zero
plt.xlim(left=0, right=212 - start_time_offset)

# Show the plot
plt.show()
