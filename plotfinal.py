import os
import pandas as pd
import matplotlib.pyplot as plt

# File paths for each curve

file_path_curve1 = 'C:/Users/imen/Desktop/final/part1_stats_history.csv'
file_path_curve2 = 'C:/Users/imen/Desktop/final/part2_stats_history.csv'
file_path_curve3 = 'C:/Users/imen/Desktop/final/part3_stats_history.csv'
file_path_curve4 = 'C:/Users/imen/Desktop/final/part4_stats_history.csv'
# Read the CSV files into pandas DataFrames

df_curve1 = pd.read_csv(file_path_curve1)
df_curve2 = pd.read_csv(file_path_curve2)
df_curve3 = pd.read_csv(file_path_curve3)
df_curve4 = pd.read_csv(file_path_curve4)
# Assuming all dataframes have a 'Timestamp' column
# Convert the timestamp column to datetime format

# Set the start point of the timestamp column to 0

df_curve1['Timestamp'] = df_curve1['Timestamp'] - df_curve1['Timestamp'][0]
df_curve2['Timestamp'] = df_curve2['Timestamp'] - df_curve2['Timestamp'][0]
df_curve3['Timestamp'] = df_curve3['Timestamp'] - df_curve3['Timestamp'][0]
df_curve4['Timestamp'] = df_curve4['Timestamp'] - df_curve4['Timestamp'][0]
# Select the data starting from 25 seconds before
start_time_offset = 0 #25

df_curve1_first_180s = df_curve1[df_curve1['Timestamp'] <= 180]
df_curve2_first_180s = df_curve2[df_curve2['Timestamp'] <= 180]
df_curve3_first_180s = df_curve3[df_curve3['Timestamp'] <= 180]
df_curve4_first_180s = df_curve4[df_curve4['Timestamp'] <= 180]
# Plot the five curves on the same graph

plt.plot(df_curve1_first_180s['Timestamp'], df_curve1_first_180s['Requests/s'], label='Curve1', linestyle='-.', color='blue')
plt.plot(df_curve2_first_180s['Timestamp'], df_curve2_first_180s['Requests/s'], label='Curve2', linestyle=':', color='red')
plt.plot(df_curve3_first_180s['Timestamp'], df_curve3_first_180s['Requests/s'], label='Curve3', linestyle='-', color='purple')
plt.plot(df_curve4_first_180s['Timestamp'], df_curve4_first_180s['Requests/s'], label='Curve4', linestyle='-', color='green')

# Add labels, title, legend, and grid
plt.xlabel('Time (seconds)')
plt.ylabel('Count')
plt.title('Multiple Curves on the Same Graph')
plt.legend()
plt.grid(True)

# Set x-axis limits to start from zero
plt.xlim(left=0, right=212 - start_time_offset)

# Show the plot
plt.show()
