import azure.functions as func
import azure.durable_functions as df
from azure.storage.blob import BlobServiceClient

myApp = df.DFApp(http_auth_level=func.AuthLevel.ANONYMOUS)

#I've chosen to build all the function on the same code to make it much more clear

@myApp.route(route="orchestrators/{functionName}")
@myApp.durable_client_input(client_name="client")
async def http_start(req: func.HttpRequest, client):
    functionName = req.route_params.get('functionName')
    instanceId = await client.start_new(functionName, None)
    response = client.create_check_status_response(req, instanceId)
    return response

@myApp.orchestration_trigger(context_name="context")
def masterorchestrator(context):

    input = yield context.call_activity("GetInputDataFn", "")

    # Fan-out: Map phase
    mapTasks = [context.call_activity("mapperFunction", {key: value}) for key, value in input.items()]
    output = yield context.task_all(mapTasks)

    # Fan-in: Shuffle phase
    shuffled = yield context.call_activity("shufflerFunction", output)

    # Fan-out: Reduce phase
    reduceTasks = [context.call_activity("reducerFunction", {key: values}) for key, values in shuffled.items()]

    # Fan-in: Aggregate the reduced results
    finalResult = yield context.task_all(reduceTasks)

    return finalResult

@myApp.activity_trigger(input_name="start")
def GetInputDataFn(start):
    connection_string = "DefaultEndpointsProtocol=https;AccountName=part5imenchihaoui;AccountKey=RefYxAjr1oolz4E+BZOpRErrcNY9mgtVqe0xXiHyjF1K0hBEmlNcIHuZDjmz1EuB2dPXcmO4JXaT+ASt0vnLIg==;EndpointSuffix=core.windows.net"
    container_name = "imenchcontainer"
    blob_service_client = BlobServiceClient.from_connection_string(connection_string)
    container_client = blob_service_client.get_container_client(container_name)

    input = {}


    blobs = container_client.list_blobs()
    offset = 0
    for blob in blobs:
        blob_client = container_client.get_blob_client(blob.name)
        blob_data = blob_client.download_blob().readall()

        # build input
        lines = blob_data.decode("utf-8").splitlines()
        for line in lines:
            input.update({str(offset): line})
            offset += 1

    return input

@myApp.activity_trigger(input_name="input")
def mapperFunction(input):
    output = []
    for key, value in input.items():
        words = value.split()
        output.extend([(word, 1) for word in words])
    return output

@myApp.activity_trigger(input_name="output")
def shufflerFunction(output):
    shuffled = {}
    for lines in output:
        for key, value in lines:
            if key not in shuffled:
                shuffled[key] = []
            shuffled[key].append(value)
    return shuffled

@myApp.activity_trigger(input_name="shuffled")
def reducerFunction(shuffled):
    result = {}
    for key, values in shuffled.items():
        total_count = sum(values)
        result[key] = total_count
    return result
